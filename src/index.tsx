import React, {useEffect, useState} from "react";
import ReactDOM from "react-dom";
import GetNumberInput from "./getNumberInput/GetNumberInput";
import {Numberbox} from "draqon-component-library";
import "./index.scss";

const App = () => {

    const min = 2;
    const max = 222;
    const [value, setValue]: any = useState("");

    const onChangeCallback = (text: any) => {
        setValue(GetNumberInput(text, min, max) );
    }

    return (
        <div>
            <Numberbox onChangeCallback={onChangeCallback} min={min} max={max} value={value} placeholderValue="enter a number" />
            <h1> Temporarily used Testing Page for Functions </h1>
        </div>
    )
}

document.getElementById("wrapper") ? ReactDOM.render(<App />, document.getElementById("wrapper")) : null;