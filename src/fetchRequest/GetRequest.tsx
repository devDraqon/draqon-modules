const GETRequest = (url: string): string => {
    const options: any = {
        method: 'GET',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: { 'Content-Type': 'application/json' },
        redirect: 'follow',
        referrerPolicy: 'no-referrer'
      };

    var res = null;
    fetch(url, options)
        .then(response => response.json())
        .then(data => res = data)
    return res;
}

export default GETRequest;