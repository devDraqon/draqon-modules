const POSTRequest = (url: string, body: any): string => {
    const options: any = {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: { 'Content-Type': 'application/json' },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify({data: body})
      };
    
    var res = null;
    fetch(url, options)
        .then(response => response.json())
        .then(data => res = data)
    return res;
}

export default POSTRequest;