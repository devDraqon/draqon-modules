const GetClassName = (classNameBase: string, additionalClassList: any): string => 
    (additionalClassList === undefined || additionalClassList.length == 0) 
        ? classNameBase 
        : classNameBase + " " + additionalClassList.join(" "+classNameBase+"__")

export default GetClassName;