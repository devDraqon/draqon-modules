const GetExpireDate = (): Date => {
    var now = new Date();
    var time = now.getTime();
    var expireTime = time + 3600 * 1000 * 24 * 366 * 10;;
    now.setTime(expireTime);
    return now;
}

export default GetExpireDate;