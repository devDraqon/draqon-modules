const GetNumberInput = (input: number, min?: number, max?: number) =>  
    (input < min) 
        ? min 
        : (input > max) 
            ? max 
            : input 

export default GetNumberInput