# Draqons Modules
This module is a collection of diverse function components

## Installation
    npm install --save draqon-modules

### Importing a function
    import {FunctionName} from 'draqon-cookie-consent';

## Available Functions
-   **GetRequest**(url: string)
-   **PostRequest**(url: string, body: any)
-   **GetClassName**(classNameBase: string, additionalClassList: any)
-   **GetCookie**(cname: string)
-   **GetNumberInput**(input: number, min?: number, max?: number)
-   **GetExpireDate**()
-   **ScrollSmoothly**()  

### Source Code
<a href="https://gitlab.com/devdraqon/draqon-modules"> visit the source code for this project here. </a>

### Known Bugs
-   no documentated known bugs

### Report Bugs and Issues
If you encounter any bugs, issues, or have improvements which you'd like to share, please choose one of the following ways to report your issue.
- <a href="https://gitlab.com/devDraqon/draqon-modules/-/issues"> Report Issue on Gitlab </a> (requires having a gitlab account).
- <a href="mailto:draqondevelops@gmail.com"> Mail your Issue </a> to me.

### Version History
-   Start of documentated version history 